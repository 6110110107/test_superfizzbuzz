import unittest
from unittest import mock
import main.guess_number as guess_number

class guessNumberTest(unittest.TestCase):

    #test case int
    @mock.patch('random.randint',return_value=int(5))
    def test_guess_int_0_to_9_should_be_5(self, guessValue):
        result = guess_number.guess_int(0,9)
        self.assertEqual(result,5,"should be 5")
        
    @mock.patch('random.randint',return_value=int(238))
    def test_guess_int_10_to_999_should_be_238(self, guessValue):
        result = guess_number.guess_int(10,999)
        self.assertEqual(result,238,"should be 238")

    @mock.patch('random.randint',return_value=int(1500000))
    def test_guess_int_1000_to_10000000_should_be_1500000(self, guessValue):
        result = guess_number.guess_int(1000,10000000)
        self.assertEqual(result,1500000,"should be 1500000")
    
    @mock.patch('random.randint',return_value=int(61))
    def test_guess_int_190_to_1_should_be_61(self, guessValue):
        result = guess_number.guess_int(190,1)
        self.assertEqual(result,61,"should be 61")

    @mock.patch('random.randint',return_value=int(0))
    def test_guess_int_0_to_0_should_be_0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0,"should be 0")

    @mock.patch('random.randint',return_value=int(19))
    def test_guess_int_19_to_19_should_be_19(self, guessValue):
        result = guess_number.guess_int(19,19)
        self.assertEqual(result,19,"should be 19")
    
    @mock.patch('random.randint',return_value=int(-99))
    def test_guess_int_minus99_to_minus99_should_be_minus99(self, guessValue):
        result = guess_number.guess_int(-99,-99)
        self.assertEqual(result,-99,"should be -99")

    @mock.patch('random.randint',return_value=int(-891))
    def test_guess_int_minus999_to_minus1_should_be_minus891(self, guessValue):
        result = guess_number.guess_int(-999,-1)
        self.assertEqual(result,-891,"should be -891")

    @mock.patch('random.randint',return_value=int(28))
    def test_guess_int_minus99_to_99_should_be_28(self, guessValue):
        result = guess_number.guess_int(-99,99)
        self.assertEqual(result,28,"should be 28")

    #test case float
    @mock.patch('random.uniform',return_value=float(5.5))
    def test_guess_float_0_to_9_should_be_7point5(self, guessValue):
        result = guess_number.guess_float(0,9)
        self.assertEqual(result,5.5,"should be 5.5")
    
    @mock.patch('random.uniform',return_value=float(5.555))
    def test_guess_float_10point1_to_99point9_should_be_5point555(self, guessValue):
        result = guess_number.guess_float(10.1,99.9)
        self.assertEqual(result,5.555,"should be 5.555")

    @mock.patch('random.uniform',return_value=float(369.963))
    def test_guess_float_minus99point999_to_99point999_should_be_minus_369point963(self, guessValue):
        result = guess_number.guess_float(-99.999,99.999)
        self.assertEqual(result,369.963,"should be -369.963")

    @mock.patch('random.uniform',return_value=float(-3.123456789))
    def test_guess_float_minus99_to_minus1_should_be_minus3point123456789(self, guessValue):
        result = guess_number.guess_float(-99,-1)
        self.assertEqual(result,-3.123456789,"should be -3.123456789")

    @mock.patch('random.randint',return_value=float(0.0))
    def test_guess_float_0_to_0_should_be_0point0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0.0,"should be 0.0")

    @mock.patch('random.randint',return_value=float(-1000.123456))
    def test_guess_float_minus1000point123456_to_minus1000point123456_should_be_minus1000point123456(self, guessValue):
        result = guess_number.guess_int(-1000.123456,-1000.123456)
        self.assertEqual(result,-1000.123456,"should be -1000.123456")

    @mock.patch('random.randint',return_value=float(100.123456))
    def test_guess_float_100point123456_to_100point123456_should_be_100point123456(self, guessValue):
        result = guess_number.guess_int(100.123456,100.123456)
        self.assertEqual(result,100.123456,"should be 100.123456")